use std::fmt::Debug;
use std::time::Duration;
use std::{io, task::Poll};

use flume::Receiver;
use futures_lite::FutureExt;
use tokio::select;
use tokio::sync::oneshot;
use tokio::{
    io::{AsyncRead, AsyncWrite},
    task::JoinHandle,
};
use tracing::{debug, event, instrument, span, trace, Instrument, Level};
use veilid_core::{Target, VeilidAPIError, VeilidAPIResult};

use crate::proto::{new_keepalive_proto, new_payload_proto};
use crate::util::CoreAPI;

/// A network stream built over Veilid app_call primitives.
pub struct AppCallStream {
    /// Unique identifier for the stream. Somewhat analogous to a
    /// dynamically-assigned port number.
    stream_id: u64,

    /// veilid-core routing context, used to write to a remote vldpipe node with
    /// app_call.
    api: Box<dyn CoreAPI>,

    /// Remote vldpipe node target "address".
    target: Target,

    /// Receiver fed vldpipe payloads via veilid-core node's update-callback,
    /// routed by vldpipe to this stream by stream_id.
    payload_receiver: Receiver<Vec<u8>>,

    /// An outbound app_call write task in progress, if any.
    write_handle: Option<JoinHandle<io::Result<usize>>>,

    /// An inbound from_veilid read task in progress, if any.
    read_handle: Option<JoinHandle<io::Result<Vec<u8>>>>,

    // Oneshot sender to shut down the keepalive task, if any.
    _keepalive_interrupter: Option<oneshot::Sender<()>>,
}

impl AppCallStream {
    pub fn new(
        stream_id: u64,
        api: Box<dyn CoreAPI>,
        target: Target,
        payload_receiver: Receiver<Vec<u8>>,
    ) -> AppCallStream {
        AppCallStream {
            stream_id,
            api,
            target,
            payload_receiver,
            write_handle: None,
            read_handle: None,
            _keepalive_interrupter: None,
        }
    }

    pub fn with_keepalive(mut self) -> Self {
        let (keepalive_interrupter, interrupt_handler) = oneshot::channel::<()>();
        tokio::spawn(Self::keepalive(
            self.stream_id,
            self.api.clone_box(),
            self.target.to_owned(),
            interrupt_handler,
        ));
        self._keepalive_interrupter = Some(keepalive_interrupter);
        self
    }

    async fn keepalive(
        stream_id: u64,
        api: Box<dyn CoreAPI>,
        target: Target,
        mut interrupt_handler: oneshot::Receiver<()>,
    ) -> VeilidAPIResult<()> {
        let mut interval = tokio::time::interval(Duration::from_secs(10));
        loop {
            select! {
                _ = interval.tick() => {
                    if let Err(e) = api.app_call(target.clone(), new_keepalive_proto(stream_id)).await {
                        debug!(stream_id, "keepalive failed: {:?}", e);
                    } else {
                        debug!(stream_id, "keepalive ok");
                    }
                }
                _ = &mut interrupt_handler => {
                    return Ok(())
                }
            }
        }
    }

    pub fn stream_id(&self) -> u64 {
        self.stream_id
    }
}

impl AsyncWrite for AppCallStream {
    #[instrument(skip(self, cx), level = Level::TRACE, ret)]
    fn poll_write(
        self: std::pin::Pin<&mut Self>,
        cx: &mut std::task::Context<'_>,
        buf: &[u8],
    ) -> std::task::Poll<Result<usize, std::io::Error>> {
        let self_mut = self.get_mut();
        let stream_id = self_mut.stream_id;
        let prev_handle = self_mut.write_handle.take();
        let (next_handle, result) = match prev_handle {
            Some(mut h) => {
                // An existing app_call in progress, check on it
                trace!(stream_id, "Some(app_call handle) running");
                if h.is_finished() {
                    trace!(stream_id, "poll_write handle finished");
                    (
                        None,
                        match h.poll(cx) {
                            Poll::Ready(Ok(result)) => Poll::Ready(result),
                            Poll::Ready(Err(e)) => {
                                if e.is_cancelled() {
                                    Poll::Ready(Err(io::Error::new(io::ErrorKind::Interrupted, e)))
                                } else {
                                    Poll::Ready(Err(io::Error::new(io::ErrorKind::Other, e)))
                                }
                            }
                            Poll::Pending => Poll::Pending,
                        },
                    )
                } else {
                    (Some(h), Poll::Pending)
                }
            }
            None => {
                // No prior write in progress, start one app_call
                trace!(stream_id, "no running handle, start new app_call");
                let n = buf.len();
                let (rc, target) = (self_mut.api.clone_box(), self_mut.target.clone());
                let msg = new_payload_proto(self_mut.stream_id, buf);
                let waker = cx.waker().clone();

                let app_call_task = async move {
                    let result = match rc
                        .app_call(target.clone(), msg)
                        .instrument(span!(Level::TRACE, "app_call"))
                        .await
                    {
                        Ok(_) => Ok(n),
                        Err(VeilidAPIError::Timeout) => Err(io::Error::new(
                            io::ErrorKind::TimedOut,
                            VeilidAPIError::Timeout,
                        )),
                        Err(VeilidAPIError::InvalidTarget) => Err(io::Error::new(
                            io::ErrorKind::AddrNotAvailable,
                            VeilidAPIError::Timeout,
                        )),
                        Err(e) => Err(io::Error::new(io::ErrorKind::Other, e)),
                    };
                    trace!(stream_id, "app_call task complete: {:?}", result);
                    waker.wake();
                    result
                };
                let handle = tokio::spawn(app_call_task);
                trace!(stream_id, "app_call task started");
                (Some(handle), Poll::Pending)
            }
        };
        self_mut.write_handle = next_handle;
        trace!(stream_id, "poll_write -> {:?}", result);
        result
    }

    #[instrument(skip_all, level = Level::TRACE, ret)]
    fn poll_flush(
        self: std::pin::Pin<&mut Self>,
        _cx: &mut std::task::Context<'_>,
    ) -> std::task::Poll<Result<(), std::io::Error>> {
        Poll::Ready(Ok(()))
    }

    #[instrument(skip_all, level = Level::TRACE, ret)]
    fn poll_shutdown(
        self: std::pin::Pin<&mut Self>,
        cx: &mut std::task::Context<'_>,
    ) -> std::task::Poll<Result<(), std::io::Error>> {
        if let Some(handle) = self.write_handle.as_ref() {
            event!(Level::TRACE, stream_id = self.stream_id, "aborting task");
            handle.abort();
        }
        cx.waker().wake_by_ref();
        Poll::Ready(Ok(()))
    }
}

impl AsyncRead for AppCallStream {
    #[instrument(skip(self, cx), level = Level::TRACE, ret)]
    fn poll_read(
        self: std::pin::Pin<&mut Self>,
        cx: &mut std::task::Context<'_>,
        buf: &mut tokio::io::ReadBuf<'_>,
    ) -> std::task::Poll<std::io::Result<()>> {
        let self_mut = self.get_mut();
        let stream_id = self_mut.stream_id();
        let prev_handle = self_mut.read_handle.take();
        let waker = cx.waker().clone();

        let (next_handle, result) = match prev_handle {
            // An existing read is in progress
            Some(mut handle) => {
                let result = match handle.poll(cx) {
                    Poll::Pending => (Some(handle), Poll::Pending),
                    Poll::Ready(Err(e)) => (
                        None,
                        if e.is_cancelled() {
                            Poll::Ready(Err(io::Error::new(io::ErrorKind::Interrupted, e)))
                        } else {
                            Poll::Ready(Err(io::Error::new(io::ErrorKind::Other, e)))
                        },
                    ),
                    Poll::Ready(Ok(Err(e))) => (None, Poll::Ready(Err(e))),
                    Poll::Ready(Ok(Ok(bytes_read))) => {
                        buf.put_slice(bytes_read.as_ref());
                        (None, Poll::Ready(Ok(())))
                    }
                };
                trace!(stream_id, "poll_read task done: {:?}", result);
                result
            }
            // No prior read in progress, start one
            None => {
                let from_veilid = self_mut.payload_receiver.clone();
                let read_handle = tokio::spawn(async move {
                    let result = match from_veilid.recv_async().await {
                        Ok(b) => Ok(b),
                        Err(e) => Err(io::Error::new(io::ErrorKind::Other, e)),
                    };
                    trace!(stream_id, "poll_read got result: {:?}", result);
                    waker.wake();
                    result
                });
                (Some(read_handle), Poll::Pending)
            }
        };
        self_mut.read_handle = next_handle;
        result
    }
}

impl Debug for AppCallStream {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("AppCallStream")
            .field("stream_id", &self.stream_id)
            .field("target", &self.target)
            .field("payload_receiver", &self.payload_receiver)
            .field("write_handle", &self.write_handle)
            .field("read_handle", &self.read_handle)
            .finish()
    }
}

#[cfg(test)]
mod tests {
    use std::time::Duration;

    use async_trait::async_trait;
    use capnp::{message::ReaderOptions, serialize};
    use flume::{unbounded, Sender};
    use tokio::io::{AsyncReadExt, AsyncWriteExt};
    use veilid_core::{OperationId, RouteId, TypedKey, ValueData, ValueSubkey, VeilidAPIResult};

    use crate::{proto, util::generic_err};

    use super::*;

    struct FakeAppCaller(Sender<(Target, Vec<u8>)>);

    #[async_trait]
    impl CoreAPI for FakeAppCaller {
        fn clone_box(&self) -> Box<dyn CoreAPI> {
            Box::new(FakeAppCaller(self.0.clone()))
        }

        async fn app_call(&self, target: Target, message: Vec<u8>) -> VeilidAPIResult<Vec<u8>> {
            self.0.send((target, message)).map_err(generic_err)?;
            Ok(vec![])
        }
        async fn app_call_reply(
            &self,
            _call_id: OperationId,
            _message: Vec<u8>,
        ) -> VeilidAPIResult<()> {
            panic!("not impl")
        }
        fn import_remote_private_route(&self, _blob: Vec<u8>) -> VeilidAPIResult<RouteId> {
            panic!("not impl")
        }
        async fn new_private_route(&self) -> VeilidAPIResult<(RouteId, Vec<u8>)> {
            panic!("not impl")
        }
        fn release_private_route(&self, _route_id: RouteId) -> VeilidAPIResult<()> {
            panic!("not impl")
        }
        async fn get_dht_value(
            &self,
            _key: TypedKey,
            _subkey: ValueSubkey,
            _force_refresh: bool,
        ) -> VeilidAPIResult<Option<ValueData>> {
            panic!("not impl")
        }
        async fn set_dht_value(
            &self,
            _key: TypedKey,
            _subkey: ValueSubkey,
            _data: Vec<u8>,
        ) -> VeilidAPIResult<Option<ValueData>> {
            panic!("not impl")
        }
    }

    fn new_stream() -> (AppCallStream, Sender<Vec<u8>>, Receiver<(Target, Vec<u8>)>) {
        let (payload_sender, payload_receiver) = unbounded();
        let (app_call_sender, app_call_receiver) = unbounded();
        (
            AppCallStream {
                stream_id: 0,
                api: Box::new(FakeAppCaller(app_call_sender)),
                target: Target::PrivateRoute(veilid_core::CryptoKey { bytes: [0; 32] }),
                payload_receiver,
                write_handle: None,
                read_handle: None,
                _keepalive_interrupter: None,
            },
            payload_sender,
            app_call_receiver,
        )
    }

    #[tokio::test]
    async fn test_read() {
        let (mut s, ps, _acr) = new_stream();
        ps.send("hello".as_bytes().to_vec())
            .expect("send to payload receiver");
        let mut buf = vec![0; 5];
        let result = s.read(&mut buf[..]).await;
        assert_eq!(result.unwrap(), 5);
        assert_eq!(buf.as_slice(), "hello".as_bytes())
    }

    #[tokio::test]
    async fn test_write() {
        let (mut s, _ps, acr) = new_stream();
        let result = s.write("hello".as_bytes()).await;
        assert_eq!(result.unwrap(), 5);
        let (_target, msg) = acr
            .recv_timeout(Duration::from_secs(1))
            .expect("recv from fake app_call");

        let reader = serialize::read_message(msg.as_slice(), ReaderOptions::new()).unwrap();
        let call_msg = reader
            .get_root::<proto::call::Reader>()
            .expect("read message")
            .which()
            .expect("parse message");
        let msg = match call_msg {
            proto::call::Which::Payload(payload) => payload,
            _ => {
                panic!("invalid message");
            }
        }
        .expect("parsed payload");

        assert_eq!(msg, "hello".as_bytes())
    }
}

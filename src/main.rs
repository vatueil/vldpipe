mod config;
mod dialer;
mod listener;
mod proto;
mod stream;
mod util;

use std::net::SocketAddr;
use std::str::FromStr;
use std::sync::Arc;
use std::{env, io, net, result};

use backoff::ExponentialBackoff;
use clap::Parser;
use flume::{unbounded, Receiver, Sender};
use thiserror::Error;
use tokio::net::{TcpListener, TcpSocket};
use tokio::select;
use tracing::{debug, error, event, info, instrument, span, trace, warn, Instrument, Level};
use tracing_subscriber::filter::EnvFilter;
use tracing_subscriber::prelude::*;
use util::VeilidCoreAPI;
use veilid_core::{CryptoTyped, DHTSchema, DHTSchemaDFLT, VeilidAPI, VeilidAPIError, VeilidUpdate};
use veilid_core::{
    DHTRecordDescriptor, KeyPair, RoutingContext, SafetySelection, SafetySpec, Sequencing,
    TypedKey, VeilidAPIResult,
};

use crate::dialer::Dialer;
use crate::listener::AppCallListener;
use crate::stream::AppCallStream;

#[derive(Debug)]
pub enum PipeSpec {
    Export {
        from_local: net::SocketAddr,
    },
    Import {
        from_remote: String,
        to_local: net::SocketAddr,
    },
}

#[derive(Error, Debug)]
pub enum AppError {
    #[error("not supported: {0}")]
    NotSupported(String),
    #[error("invalid socket address: {0}")]
    ParseAddr(#[from] net::AddrParseError),
    #[error("io error: {0}")]
    IO(#[from] io::Error),
    #[error("api error: {0}")]
    API(#[from] VeilidAPIError),
    #[error("error: {0}")]
    Other(String),
}

pub type Result<T> = result::Result<T, AppError>;

#[derive(Parser, Debug)]
#[command(name = "vldpipe")]
#[command(bin_name = "vldpipe")]
pub struct Cli {
    pub src: String,
    pub dst: Option<String>,

    #[arg(long)]
    pub app_dir: Option<String>,
}

impl Cli {
    #[instrument(level = Level::DEBUG, ret)]
    pub fn app_dir(&self) -> String {
        match &self.app_dir {
            Some(app_dir) => app_dir.to_owned(),
            None => format!(
                "{}/{}",
                env::home_dir().unwrap_or(".".into()).to_string_lossy(),
                ".vldpipe"
            ),
        }
    }
}

impl TryInto<PipeSpec> for Cli {
    type Error = AppError;

    #[instrument(level = Level::DEBUG, ret, err(level = Level::ERROR))]
    fn try_into(self) -> Result<PipeSpec> {
        match self.dst.as_ref() {
            None => Ok(PipeSpec::Export {
                from_local: net::SocketAddr::from_str(self.src.as_str())?,
            }),
            Some(dst) => Ok(PipeSpec::Import {
                from_remote: self.src.clone(),
                to_local: net::SocketAddr::from_str(dst)?,
            }),
        }
    }
}

#[tokio::main]
async fn main() {
    tracing_subscriber::registry()
        .with(tracing_subscriber::fmt::layer())
        .with(
            EnvFilter::builder()
                .with_default_directive("vldpipe=info".parse().unwrap())
                .from_env_lossy(),
        )
        .init();

    if let Err(e) = run().await {
        error!("{}", e);
        std::process::exit(1)
    }
}

async fn run() -> Result<()> {
    let cli = Cli::parse();

    let app_dir = cli.app_dir();

    // Create client api state change pipe
    let (node_sender, node_receiver): (
        Sender<veilid_core::VeilidUpdate>,
        Receiver<veilid_core::VeilidUpdate>,
    ) = unbounded();

    // Create VeilidCore setup
    let update_callback = Arc::new(move |change: veilid_core::VeilidUpdate| {
        let _ = node_sender.send(change);
    });
    let config_callback = Arc::new(move |key| config::config_callback(app_dir.clone(), key));

    let api: veilid_core::VeilidAPI =
        veilid_core::api_startup(update_callback, config_callback).await?;
    api.attach().await?;

    // Wait for network to be up
    async {
        loop {
            let res = node_receiver.recv_async().await;
            match res {
                Ok(VeilidUpdate::Attachment(attachment)) => {
                    info!("{:?}", attachment);
                    if attachment.public_internet_ready {
                        return Ok(());
                    }
                }
                Ok(VeilidUpdate::Config(_)) => {}
                Ok(VeilidUpdate::Log(_)) => {}
                Ok(VeilidUpdate::Network(_)) => {}
                Ok(u) => {
                    trace!("{:?}", u);
                }
                Err(e) => {
                    return Err(AppError::Other(e.to_string()));
                }
            };
        }
    }
    .instrument(span!(Level::INFO, "wait for network available"))
    .await?;

    let pipe_spec: PipeSpec = cli.try_into()?;
    match pipe_spec {
        PipeSpec::Export { from_local } => run_export(api, node_receiver, from_local).await,
        PipeSpec::Import {
            from_remote,
            to_local,
        } => run_import(api, node_receiver, from_remote, to_local).await,
    }
}

#[instrument(skip(api, node_receiver), level = Level::DEBUG, err(level = Level::ERROR))]
async fn run_import(
    api: VeilidAPI,
    node_receiver: Receiver<VeilidUpdate>,
    from_remote: String,
    to_local: SocketAddr,
) -> Result<()> {
    let local_ln = TcpListener::bind(to_local).await?;

    let routing_context = api.routing_context().with_custom_privacy(privacy())?;
    let mut dialer = Dialer::new(VeilidCoreAPI::new(&routing_context), node_receiver)?;

    let remote_dht = open_dht_record(&routing_context, from_remote.to_owned()).await?;

    let result: Result<()> = async {
        loop {
            let (local_stream, local_addr) = local_ln.accept().await?;
            event!(Level::DEBUG, connection_from = local_addr.to_string());

            let dial_result = dialer.dial(remote_dht.clone()).await;
            let remote_stream = match dial_result {
                Ok(rs) => rs,
                Err(e) => {
                    error!("dial failed: {:?}", e);
                    continue;
                }
            };
            trace!(stream_id = remote_stream.stream_id(), "dial ok");
            tokio::spawn(forward(local_stream, remote_stream));
            debug!("started forward");
        }
    }
    .await;

    // Cleanup
    if let Err(e) = dialer.close().await {
        trace!("failed to close dialer: {:?}", e);
    }
    if let Err(e) = routing_context.close_dht_record(*remote_dht.key()).await {
        warn!("failed to close DHT record: {:?}", e);
    }
    result
}

#[instrument(skip(api, node_receiver), level = Level::DEBUG, ret, err(level = Level::ERROR))]
async fn run_export(
    api: VeilidAPI,
    node_receiver: Receiver<VeilidUpdate>,
    from_local: SocketAddr,
) -> Result<()> {
    // Create and store, or load DHT key for this local address
    // TODO: accept a 'name' argument for this
    let routing_context = api.routing_context().with_custom_privacy(privacy())?;
    let ln_inbound_dht = match load_dht_key(&api, from_local.to_string().as_str()).await? {
        Some((key, owner)) => routing_context.open_dht_record(key, Some(owner)).await?,
        None => {
            let new_dht = routing_context
                .create_dht_record(DHTSchema::DFLT(DHTSchemaDFLT { o_cnt: 1 }), None)
                .instrument(span!(Level::DEBUG, "create_dht_record"))
                .await?;
            store_dht_key(
                &api,
                from_local.to_string().as_str(),
                new_dht.key(),
                KeyPair::new(
                    new_dht.owner().to_owned(),
                    new_dht.owner_secret().unwrap().to_owned(),
                ),
            )
            .await?;
            new_dht
        }
    };

    // Print the DHT key, this is what clients can "connect()" to
    println!("{}", ln_inbound_dht.key());

    let mut ln = AppCallListener::new(VeilidCoreAPI::new(&routing_context), node_receiver);
    ln.bind(ln_inbound_dht.to_owned()).await?;
    let result = async {
        loop {
            let remote = ln.accept().await?;
            let socket = TcpSocket::new_v4()?;
            trace!(stream_id = remote.stream_id(), "starting forward");
            tokio::spawn(forward(socket.connect(from_local).await?, remote));
        }
    }
    .instrument(tracing::debug_span!("accept_and_forward"))
    .await;

    // Cleanup
    if let Err(e) = ln.close().await {
        debug!("failed to close listener: {:?}", e);
    }
    if let Err(e) = routing_context
        .close_dht_record(*ln_inbound_dht.key())
        .await
    {
        warn!("failed to close DHT record: {:?}", e);
    }

    result
}

#[instrument(skip_all, level = Level::DEBUG, ret, err(level = Level::ERROR))]
async fn forward(
    mut local_stream: tokio::net::TcpStream,
    remote_stream: AppCallStream,
) -> Result<()> {
    let (mut local_read, mut local_write) = local_stream.split();
    let (mut remote_read, mut remote_write) = tokio::io::split(remote_stream);
    select! {
        res = tokio::io::copy(&mut remote_read, &mut local_write) => res.map(|_| ()).map_err(|e| AppError::IO(e)),
        res = tokio::io::copy(&mut local_read, &mut remote_write) => res.map(|_| ()).map_err(|e| AppError::IO(e)),
    }
}

#[instrument(skip(api), level = Level::DEBUG, ret, err(level = Level::ERROR))]
async fn load_dht_key(api: &VeilidAPI, name: &str) -> VeilidAPIResult<Option<(TypedKey, KeyPair)>> {
    let db = api.table_store()?.open("vldpipe", 2).await?;
    let key = db.load_json::<TypedKey>(0, name.as_bytes()).await?;
    let owner = db.load_json::<KeyPair>(1, name.as_bytes()).await?;
    Ok(match (key, owner) {
        (Some(k), Some(o)) => Some((k, o)),
        _ => None,
    })
}

#[instrument(skip(api, key, owner), level = Level::DEBUG, ret, err(level = Level::ERROR))]
async fn store_dht_key(
    api: &VeilidAPI,
    name: &str,
    key: &TypedKey,
    owner: KeyPair,
) -> VeilidAPIResult<()> {
    let db = api.table_store()?.open("vldpipe", 2).await?;
    db.store_json(0, name.as_bytes(), key).await?;
    db.store_json(1, name.as_bytes(), &owner).await
}

#[instrument(skip(routing_context), level = Level::DEBUG, ret, err(level = Level::ERROR))]
async fn open_dht_record(
    routing_context: &RoutingContext,
    from_remote: String,
) -> VeilidAPIResult<DHTRecordDescriptor> {
    backoff::future::retry_notify(
        ExponentialBackoff::default(),
        || async {
            Ok(routing_context
                .open_dht_record(CryptoTyped::from_str(from_remote.as_str())?, None)
                .await?)
        },
        |e, dur| {
            warn!("get_remote_route failed: {:?} after {:?}", e, dur);
        },
    )
    .await
}

#[instrument(level = Level::DEBUG, ret)]
fn privacy() -> SafetySelection {
    SafetySelection::Safe(SafetySpec {
        sequencing: Sequencing::EnsureOrdered,
        stability: veilid_core::Stability::LowLatency,
        preferred_route: None,
        hop_count: 3,
    })
}

use std::collections::HashMap;

use capnp::{message::ReaderOptions, serialize};
use flume::{unbounded, Receiver, Sender};
use tokio::task::JoinHandle;
use tracing::{debug, debug_span, instrument, span, trace, trace_span, warn, Instrument, Level};
use veilid_core::{DHTRecordDescriptor, Target, VeilidAPIResult, VeilidUpdate};

use crate::{
    proto::{self, new_connect_proto, new_keepalive_proto, new_payload_proto},
    stream::AppCallStream,
    util::{generic_err, CoreAPI},
};

pub struct AppCallListener {
    api: Box<dyn CoreAPI>,
    from_node: Receiver<VeilidUpdate>,
    client_stream_receiver: Option<Receiver<AppCallStream>>,
    handler: Option<JoinHandle<VeilidAPIResult<()>>>,
}

impl AppCallListener {
    pub fn new(api: Box<dyn CoreAPI>, from_node: Receiver<VeilidUpdate>) -> AppCallListener {
        AppCallListener {
            api: api.clone_box(),
            from_node,
            client_stream_receiver: None,
            handler: None,
        }
    }

    #[instrument(skip_all, level = Level::DEBUG, err(level = Level::ERROR))]
    pub async fn bind(&mut self, addr: DHTRecordDescriptor) -> VeilidAPIResult<()> {
        let (inbound_key, inbound_blob) = self.api.new_private_route().await?;
        debug!(
            route = inbound_key.to_string(),
            "created private route for bind"
        );
        self.api
            .set_dht_value(addr.key().to_owned(), 0, inbound_blob.clone())
            .await?;

        let (client_stream_sender, client_stream_receiver): (
            Sender<AppCallStream>,
            Receiver<AppCallStream>,
        ) = unbounded();
        let (api, from_node) = (self.api.clone_box(), self.from_node.clone());
        self.client_stream_receiver = Some(client_stream_receiver);
        self.handler = Some(tokio::spawn(async move {
            Self::handle_node_updates(api, from_node, client_stream_sender).await
        }));
        Ok(())
    }

    #[instrument(skip_all, level = Level::TRACE, err(level = Level::ERROR))]
    pub async fn accept(&self) -> VeilidAPIResult<AppCallStream> {
        let client_stream_receiver = match self.client_stream_receiver.as_ref() {
            Some(r) => r,
            None => return Err(generic_err("listener is not bound")),
        };
        let client_stream = client_stream_receiver
            .recv_async()
            .await
            .map_err(generic_err)?;
        trace!(stream_id = client_stream.stream_id(), "got stream_id");
        Ok(client_stream)
    }

    #[instrument(skip_all, level = Level::TRACE, err(level = Level::DEBUG))]
    pub async fn close(mut self) -> VeilidAPIResult<()> {
        let handler = match self.handler.take() {
            Some(h) => h,
            None => {
                return Err(generic_err("listener is not bound"));
            }
        };
        handler.abort();
        self.client_stream_receiver.take();
        handler.await.map_err(generic_err)?
    }

    #[instrument(skip_all, level = Level::TRACE, err(level = Level::ERROR))]
    async fn handle_node_updates(
        api: Box<dyn CoreAPI>,
        from_node: Receiver<VeilidUpdate>,
        client_stream_sender: Sender<AppCallStream>,
    ) -> VeilidAPIResult<()> {
        let mut stream_senders = HashMap::new();
        loop {
            // Receive and decode app_call from veilid-core updates
            let (stream_id, call_id, maybe_connect, maybe_payload) =
                match from_node.recv_async().await.map_err(generic_err) {
                    Ok(VeilidUpdate::AppCall(app_call)) => {
                        let app_call_id = app_call.id();
                        trace!(call_id = u64::from(app_call_id), "got app_call");
                        let reader =
                            serialize::read_message(app_call.message(), ReaderOptions::new())
                                .unwrap();
                        let call_msg = reader.get_root::<proto::call::Reader>().unwrap();
                        match call_msg.which() {
                            Ok(proto::call::Which::Connect(Ok(connect))) => (
                                app_call_id.into(),
                                app_call_id,
                                Some(connect.to_owned()),
                                None,
                            ),
                            Ok(proto::call::Which::Payload(Ok(payload))) => (
                                call_msg.get_id(),
                                app_call_id,
                                None,
                                Some(payload.to_owned()),
                            ),
                            Ok(proto::call::Which::Keepalive(())) => {
                                (call_msg.get_id(), app_call_id, None, None)
                            }
                            Ok(proto::call::Which::Connect(Err(e))) => return Err(generic_err(e)),
                            Ok(proto::call::Which::Payload(Err(e))) => return Err(generic_err(e)),
                            Err(e) => return Err(generic_err(e)),
                        }
                    }
                    Ok(VeilidUpdate::Shutdown) => return Ok(()),
                    Ok(_) => continue,
                    Err(e) => return Err(e),
                };

            // Dispatch valid app_call
            let result = async {
                let maybe_stream = stream_senders.get(&stream_id).cloned();
                match (maybe_connect, maybe_payload) {
                    (Some(connect), _) => {
                        // A new connection request. Create an AppCallStream for it
                        // and send it to be accept()-ed.
                        let conn_outbound_target = Target::PrivateRoute(
                            api.import_remote_private_route(connect.to_vec())?,
                        );
                        debug!(
                            stream_id,
                            conn_outbound_target = format!("{:?}", conn_outbound_target),
                            "imported connection outbound target"
                        );

                        let (conn_inbound_key, conn_inbound_blob) = api.new_private_route().await?;
                        // TODO: keep track of key for cleanup
                        debug!(
                            route = conn_inbound_key.to_string(),
                            call_id = u64::from(call_id),
                            "created private route for accepted connection"
                        );

                        // Create a sender / receiver for the forward
                        let (stream_sender, stream_receiver): (Sender<Vec<u8>>, Receiver<Vec<u8>>) =
                            unbounded();
                        stream_senders.insert(stream_id, (conn_inbound_key, stream_sender));

                        let remote_stream = AppCallStream::new(
                            stream_id,
                            api.clone_box(),
                            conn_outbound_target,
                            stream_receiver,
                        )
                        .with_keepalive();
                        client_stream_sender
                            .send(remote_stream)
                            .map_err(generic_err)?;

                        let connect_reply = new_connect_proto(stream_id, &conn_inbound_blob);
                        let reply_api = api.clone_box();
                        tokio::spawn(async move {
                            reply_api
                                .app_call_reply(call_id, connect_reply)
                                .instrument(debug_span!("connect: app_call_reply"))
                                .await
                        });
                    }
                    (None, Some(payload)) => {
                        // Route and send payload to the right stream
                        if let Some((route_id, stream_sender)) = maybe_stream {
                            if let Err(e) = stream_sender.send(payload.to_vec()) {
                                debug!(stream_id, "stream receiver is gone, removing sender");
                                stream_senders.remove(&stream_id);
                                if let Err(e) = api.release_private_route(route_id) {
                                    warn!(
                                        route_id = route_id.to_string(),
                                        "failed to release private route: ${:?}", e
                                    );
                                }
                                return Err(generic_err(e));
                            } else {
                                trace!(stream_id, "sent payload to stream");
                            }
                        }
                        let reply_api = api.clone_box();
                        tokio::spawn(async move {
                            reply_api
                                .app_call_reply(call_id, new_payload_proto(stream_id, &[]))
                                .instrument(debug_span!("payload: app_call_reply"))
                                .await
                        });
                    }
                    (None, None) => {
                        let reply_api = api.clone_box();
                        tokio::spawn(async move {
                            reply_api
                                .app_call_reply(call_id, new_keepalive_proto(stream_id))
                                .instrument(trace_span!("keepalive: app_call_reply"))
                                .await
                        });
                    }
                };
                Ok(())
            }
            .instrument(span!(Level::TRACE, "dispatch stream"))
            .await;
            if let Err(e) = result {
                trace!("dispatch failed: {:?}", e);
            }
        }
    }
}

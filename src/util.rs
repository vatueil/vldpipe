use async_trait::async_trait;
use tracing::{instrument, Level};
use veilid_core::{
    OperationId, RouteId, RoutingContext, Target, TypedKey, ValueData, ValueSubkey, VeilidAPIError,
    VeilidAPIResult,
};

pub fn generic_err<T: ToString>(e: T) -> VeilidAPIError {
    VeilidAPIError::Generic {
        message: e.to_string(),
    }
}

#[async_trait]
pub trait CoreAPI: Send + Sync {
    fn clone_box(&self) -> Box<dyn CoreAPI>;
    async fn app_call(&self, target: Target, message: Vec<u8>) -> VeilidAPIResult<Vec<u8>>;
    async fn app_call_reply(&self, call_id: OperationId, message: Vec<u8>) -> VeilidAPIResult<()>;
    fn import_remote_private_route(&self, blob: Vec<u8>) -> VeilidAPIResult<RouteId>;
    async fn new_private_route(&self) -> VeilidAPIResult<(RouteId, Vec<u8>)>;
    fn release_private_route(&self, route_id: RouteId) -> VeilidAPIResult<()>;
    async fn get_dht_value(
        &self,
        key: TypedKey,
        subkey: ValueSubkey,
        force_refresh: bool,
    ) -> VeilidAPIResult<Option<ValueData>>;
    async fn set_dht_value(
        &self,
        key: TypedKey,
        subkey: ValueSubkey,
        data: Vec<u8>,
    ) -> VeilidAPIResult<Option<ValueData>>;
}

pub struct VeilidCoreAPI(RoutingContext);

impl VeilidCoreAPI {
    pub fn new(routing_context: &RoutingContext) -> Box<dyn CoreAPI> {
        Box::new(VeilidCoreAPI(routing_context.clone()))
    }
}

#[async_trait]
impl CoreAPI for VeilidCoreAPI {
    fn clone_box(&self) -> Box<dyn CoreAPI> {
        Box::new(VeilidCoreAPI(self.0.clone()))
    }

    #[instrument(skip(self, message), level = Level::TRACE, err(level = Level::ERROR))]
    async fn app_call(&self, target: Target, message: Vec<u8>) -> VeilidAPIResult<Vec<u8>> {
        self.0.app_call(target, message).await
    }

    #[instrument(skip(self, message), level = Level::TRACE, err(level = Level::ERROR))]
    async fn app_call_reply(&self, call_id: OperationId, message: Vec<u8>) -> VeilidAPIResult<()> {
        self.0.api().app_call_reply(call_id, message).await
    }

    #[instrument(skip_all, level = Level::DEBUG, err(level = Level::ERROR))]
    fn import_remote_private_route(&self, blob: Vec<u8>) -> VeilidAPIResult<RouteId> {
        self.0.api().import_remote_private_route(blob)
    }

    #[instrument(skip_all, level = Level::DEBUG, err(level = Level::ERROR))]
    async fn new_private_route(&self) -> VeilidAPIResult<(RouteId, Vec<u8>)> {
        self.0.api().new_private_route().await
    }

    #[instrument(skip(self), level = Level::DEBUG, err(level = Level::ERROR))]
    fn release_private_route(&self, route_id: RouteId) -> VeilidAPIResult<()> {
        self.0.api().release_private_route(route_id)
    }

    #[instrument(skip(self, key), level = Level::DEBUG, err(level = Level::ERROR))]
    async fn get_dht_value(
        &self,
        key: TypedKey,
        subkey: ValueSubkey,
        force_refresh: bool,
    ) -> VeilidAPIResult<Option<ValueData>> {
        self.0.get_dht_value(key, subkey, force_refresh).await
    }

    #[instrument(skip(self, key, data), level = Level::DEBUG, err(level = Level::ERROR))]
    async fn set_dht_value(
        &self,
        key: TypedKey,
        subkey: ValueSubkey,
        data: Vec<u8>,
    ) -> VeilidAPIResult<Option<ValueData>> {
        self.0.set_dht_value(key, subkey, data).await
    }
}

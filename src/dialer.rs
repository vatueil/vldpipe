use std::collections::HashMap;

use capnp::message::ReaderOptions;
use capnp::serialize;
use flume::{unbounded, Receiver, Sender};
use tokio::select;
use tokio::task::JoinHandle;
use tracing::{debug, debug_span, instrument, trace, trace_span, Instrument, Level, warn};
use veilid_core::{DHTRecordDescriptor, RouteId, Target, VeilidAPIResult, VeilidUpdate};

use crate::proto::{self, new_keepalive_proto};
use crate::proto::{new_connect_proto, new_payload_proto};
use crate::stream::AppCallStream;
use crate::util::{generic_err, CoreAPI};

pub struct Dialer {
    api: Box<dyn CoreAPI>,
    inbound_route: Option<(RouteId, Vec<u8>)>,
    payload_sender_sender: Sender<(u64, Sender<Vec<u8>>)>,
    _handler: JoinHandle<VeilidAPIResult<()>>,
}

impl Dialer {
    #[instrument(skip_all, level = Level::DEBUG, err(level = Level::ERROR))]
    pub fn new(
        api: Box<dyn CoreAPI>,
        from_node: Receiver<VeilidUpdate>,
    ) -> VeilidAPIResult<Dialer> {
        let (payload_sender_sender, payload_sender_receiver) = unbounded();
        Ok(Dialer {
            api: api.clone_box(),
            inbound_route: None,
            payload_sender_sender,
            _handler: tokio::spawn(async move {
                Self::handle_node_updates(api, from_node, payload_sender_receiver).await
            }),
        })
    }

    #[instrument(skip_all, level = Level::TRACE, ret, err(level = Level::ERROR))]
    async fn handle_node_updates(
        api: Box<dyn CoreAPI>,
        from_node: Receiver<VeilidUpdate>,
        payload_sender_receiver: Receiver<(u64, Sender<Vec<u8>>)>,
    ) -> VeilidAPIResult<()> {
        let mut stream_senders: HashMap<u64, Sender<Vec<u8>>> = HashMap::new();
        loop {
            select! {
                node_res = from_node.recv_async() => {
                    let (stream_id, call_id, maybe_payload) = match node_res {
                        Ok(VeilidUpdate::AppCall(app_call)) => {
                            match read_call_message(app_call.message()) {
                                Ok((id, Some(payload))) => (id, app_call.id(), Some(payload)),
                                Ok((id, None)) => (id, app_call.id(), None),
                                Err(e) => {
                                    debug!(call_id = u64::from(app_call.id()), "invalid message: {:?}", e);
                                    continue
                                }
                            }
                        }
                        Ok(VeilidUpdate::Shutdown) => {
                            debug!("shutdown received");
                            return Ok(());
                        }
                        Ok(_) => continue,
                        Err(e) => return Err(generic_err(e)),
                    };
                    let sender = match stream_senders.get(&stream_id) {
                        Some(s) => s,
                        None => {
                            trace!(stream_id, "app_call for removed stream");
                            continue
                        }
                    };
                    let reply_api = api.clone_box();

                    // route message to the right forward
                    match maybe_payload {
                        Some(payload) => {
                            if !payload.is_empty() {
                                if let Err(_) =
                                    sender.send(payload.to_vec())
                                {
                                    debug!(stream_id, "forward is gone, removing sender");
                                    stream_senders.remove(&stream_id);
                                    continue
                                } else {
                                    trace!(stream_id, "sent payload to forwarding stream");
                                }
                            }
                            tokio::spawn(async move {
                                reply_api.app_call_reply(call_id, new_payload_proto(stream_id, &[]))
                                    .instrument(debug_span!("payload: app_call_reply")).await
                            });
                        }
                        None => {
                            tokio::spawn(async move {
                                reply_api.app_call_reply(call_id, new_keepalive_proto(stream_id))
                                    .instrument(trace_span!("keepalive: app_call_reply")).await
                            });
                        }
                    };
                }
                ps_res = payload_sender_receiver.recv_async() => {
                    match ps_res {
                        Ok((stream_id, stream_sender)) => {
                            debug!(stream_id, "registered sender for stream");
                            stream_senders.insert(stream_id, stream_sender);
                        }
                        Err(e) => return Err(generic_err(e)),
                    }
                }
            }
        }
    }

    #[instrument(skip_all, level = Level::DEBUG, ret, err(level = Level::ERROR))]
    pub async fn dial(&mut self, addr: DHTRecordDescriptor) -> VeilidAPIResult<AppCallStream> {
        let connect_target = self.resolve(addr).await?;
        trace!("dialer: resolved {:?}", connect_target);

        let (stream_id, outbound_target) = match self.connect(connect_target).await {
            Ok(result) => result,
            Err(e) => {
                let _ = self.close();
                return Err(e)
            }
        };
        trace!(stream_id, "connected stream");

        let (payload_sender, payload_receiver) = unbounded();
        self.payload_sender_sender
            .send((stream_id, payload_sender))
            .map_err(generic_err)?;
        Ok(AppCallStream::new(
            stream_id,
            self.api.clone_box(),
            outbound_target,
            payload_receiver,
        )
        .with_keepalive())
    }

    #[instrument(skip_all, level = Level::DEBUG, ret, err(level = Level::ERROR))]
    async fn resolve(&self, addr: DHTRecordDescriptor) -> VeilidAPIResult<Target> {
        let remote_entry = self
            .api
            .get_dht_value(addr.key().to_owned(), 0, true)
            .await?;
        let remote_blob = match remote_entry {
            Some(entry) => entry.data().to_vec(),
            None => {
                return Err(generic_err("address has not published a route"));
            }
        };
        Ok(Target::PrivateRoute(
            self.api.import_remote_private_route(remote_blob)?,
        ))
    }

    #[instrument(skip(self), level = Level::DEBUG, ret, err(level = Level::ERROR))]
    async fn connect(&mut self, target: Target) -> VeilidAPIResult<(u64, Target)> {
        let (_, inbound_blob) = match &self.inbound_route {
            Some(route) => route.clone(),
            None => {
                let route = self.api.new_private_route().await?;
                self.inbound_route = Some(route.clone());
                route
            }
        };

        let connect_msg = new_connect_proto(0, &inbound_blob);
        let resp = self
            .api
            .app_call(target, connect_msg)
            .instrument(debug_span!("connect: app_call"))
            .await?;
        let reader =
            serialize::read_message(resp.as_slice(), ReaderOptions::new()).map_err(generic_err)?;
        let call = reader
            .get_root::<proto::call::Reader>()
            .map_err(generic_err)?;

        let call_id: u64 = call.get_id().into();
        let outbound_blob = match call.which() {
            Ok(proto::call::Which::Connect(Ok(route))) => route.to_owned(),
            Ok(_) => {
                return Err(generic_err("invalid connect response"));
            }
            Err(e) => {
                return Err(generic_err(e));
            }
        };
        let outbound_key = self.api.import_remote_private_route(outbound_blob)?;
        Ok((call_id, Target::PrivateRoute(outbound_key)))
    }

    #[instrument(skip_all, level = Level::DEBUG, ret, err(level = Level::ERROR))]
    pub async fn close(&mut self) -> VeilidAPIResult<()> {
        if let Some((route_id, _)) = self.inbound_route.take() {
            if let Err(e) = self.api.release_private_route(route_id) {
                warn!(
                    route = route_id.to_string(),
                    "failed to release private route: {:?}", e
                );
            };
        }
        Ok(())
    }
}

#[instrument(skip_all, level = Level::TRACE, err(level = Level::ERROR))]
fn read_call_message(msg: &[u8]) -> VeilidAPIResult<(u64, Option<Vec<u8>>)> {
    let reader = serialize::read_message(msg, ReaderOptions::new()).map_err(generic_err)?;
    let call = reader
        .get_root::<proto::call::Reader>()
        .map_err(generic_err)?;
    match call.which() {
        Ok(proto::call::Which::Payload(Ok(p))) => Ok((call.get_id(), Some(p.to_vec()))),
        Ok(proto::call::Which::Keepalive(())) => Ok((call.get_id(), None)),
        Ok(_) => Err(generic_err("invalid call")),
        Err(e) => Err(generic_err(e)),
    }
}

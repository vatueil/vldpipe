use capnp::{message::Builder, serialize};

use crate::proto;

pub fn new_payload_proto(call_id: u64, contents: &[u8]) -> Vec<u8> {
    let mut msg_builder = Builder::new_default();
    let mut msg = msg_builder.init_root::<proto::call::Builder>();
    msg.set_id(call_id);
    msg.set_payload(contents);
    return serialize::write_message_to_words(&msg_builder);
}

pub fn new_connect_proto(call_id: u64, route: &[u8]) -> Vec<u8> {
    let mut conn_builder = Builder::new_default();
    let mut conn = conn_builder.init_root::<proto::call::Builder>();
    conn.set_id(call_id);
    conn.set_connect(route);
    return serialize::write_message_to_words(&conn_builder);
}

pub fn new_keepalive_proto(call_id: u64) -> Vec<u8> {
    let mut conn_builder = Builder::new_default();
    let mut conn = conn_builder.init_root::<proto::call::Builder>();
    conn.set_id(call_id);
    conn.set_keepalive(());
    return serialize::write_message_to_words(&conn_builder);
}
